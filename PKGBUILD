# Maintainer: Mark Wagie <mark at manjaro dot org>
# Contributor: Bernhard Landauer <bernhard@manjaro.org>

pkgname=tlpui
_app_id=com.github.d4nj1.tlpui
pkgver=1.8.0
pkgrel=1
pkgdesc="A GTK user interface for TLP written in Python"
arch=('any')
url="https://github.com/d4nj1/TLPUI"
license=('GPL-2.0-or-later')
depends=(
  'gtk3'
  'python-gobject'
  'python-yaml'
  'tlp'
)
makedepends=(
  'git'
  'python-build'
  'python-installer'
  'python-poetry-core'
  'python-wheel'
)
checkdepends=(
  'appstream'
  'python-pytest'
)
source=("git+https://github.com/d4nj1/TLPUI.git#tag=$pkgname-$pkgver")
sha256sums=('54b28b924525572c53ba6de3661dd03f582172a334fb5bc8d55dffa2e272cf42')

build() {
  cd TLPUI
  python -m build --wheel --no-isolation
}

check() {
  cd TLPUI
  desktop-file-validate "$pkgname.desktop"
  appstreamcli validate --no-net "AppImage/${_app_id}.appdata.xml"

  pytest
}

package() {
  cd TLPUI
  python -m installer --destdir="$pkgdir" dist/*.whl

  install -Dm644 "$pkgname.desktop" "$pkgdir/usr/share/applications/${_app_id}.desktop"
  install -Dm644 "AppImage/${_app_id}.appdata.xml" -t "$pkgdir/usr/share/metainfo/"
  install -Dm644 "$pkgname/icons/themeable/hicolor/scalable/apps/$pkgname.svg" \
    "$pkgdir/usr/share/icons/hicolor/scalable/apps/${_app_id}.svg"

  for i in 16 32 48 64 128 96 128 256; do
    install -Dm644 "$pkgname/icons/themeable/hicolor/${i}x${i}/apps/$pkgname.png" \
      "$pkgdir/usr/share/icons/hicolor/${i}x${i}/apps/${_app_id}.png"
  done
}
